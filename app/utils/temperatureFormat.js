export const getTemperatureInCelsius = (temp) => {
    return temp - 273.15
}
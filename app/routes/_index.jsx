import { Backdrop, Card, CircularProgress, Typography } from "@mui/material";
import { useDebugValue, useEffect, useState } from "react";
import Header from "../components/Header";
import Dashboard from "../components/Dashboard";
import { ThemeProvider } from "@emotion/react";
import theme from "../theme";
import {
  getSearchCityWeather,
  getSearchCityWeatherByLocation,
} from "../redux/weatherApi/weatherThunk";
import { useDispatch, useSelector } from "react-redux";
import {
  getForecast,
  getForecastByLocation,
} from "../redux/forecastApi/forecastThunk";

export const meta = () => {
  return [
    { title: "New Remix App" },
    { name: "description", content: "Welcome to Remix!" },
  ];
};

export default function Index() {
  const dispatch = useDispatch();
  const [location, setLocation] = useState("");
  // const [apiCall, setApiCall] = useState(false);
  // const [weatherData, setWeatherData] = useState(null);
  // const [forecastData, setForecastData] = useState(null);
  // const API_key = "1c3b6d45630cafa7030e5cdcfc2150de";
  const data = useSelector((state) => state.weather.data);
  const { loading } = useSelector((state) => state.weather);
  const forecastData = useSelector((state) => state.forecast.data);
  // console.log("forecastData", forecastData);

  const handleWeatherSearch = (e) => {
    e.preventDefault();
    // console.log("location", location);
    dispatch(getSearchCityWeather(location));
    dispatch(getForecast(location));
  };

  const successCallback = async (position) => {
    // console.log(position);
    const temp = await dispatch(
      getSearchCityWeatherByLocation(position.coords)
    );
    setLocation(temp?.payload?.name);
    // console.log("TEMP", temp);
    dispatch(getForecast(temp?.payload?.name));

    // dispatch(getForecastByLocation(position.coord));
  };

  const errorCallback = (error) => {
    console.log(error);
  };
  useEffect(() => {
    if (typeof window !== "undefined") {
      navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
    }
  }, []);
  const handleYourLocationWeather = (e) => {
    e.preventDefault();
    setLocation("");
    navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <Header
          location={location}
          setLocation={setLocation}
          handleWeatherSearch={handleWeatherSearch}
          handleYourLocationWeather={handleYourLocationWeather}
        />
        {loading ? (
          <Backdrop
            sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={loading}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        ) : data && data?.name ? (
          <Dashboard
            data={data}
            handleWeatherSearch={handleWeatherSearch}
            forecastData={forecastData}
          />
        ) :   <Typography textAlign={"center"} variant="h4" >
            No results found. Please try a different search.
          </Typography> }
      </ThemeProvider>
    </>
  );
}

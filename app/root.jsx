import { cssBundleHref } from "@remix-run/css-bundle";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import theme from "./theme"
import "./assets/css/App.css";
import { Provider } from "react-redux";
import { store } from "./redux/store";


export default function App() {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
        <link rel="stylesheet" type="text/css" href="/assets/css/App.css" />
      </head>
      <Provider store={store}>
      <body style={{margin:0}}>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
      </Provider>
    </html>
  );
}


import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  typography: {
    fontFamily: 'Arial', 
    color: '#5E82F4', 
  },
});

export default theme;

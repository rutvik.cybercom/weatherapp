import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import API_KEY from "../../../Constants/Api";


export const getForecast = createAsyncThunk(
	"weather/getForecast",
	async (location) => {
        // console.log("CORDINFATEs IN APICCALL",lat,lon)
		try {
			const res = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${location}&appid=${API_KEY}`);
			// const res = await axios.get(`api.openweathermap.org/data/2.5/forecast?lat=${coordinates.lat}&lon=${coordinates.lon}&appid=${API_KEY}`);
			// const res = await axios.get(`api.openweathermap.org/data/2.5/forecast?lat=44.34&lon=10.99&appid=${API_KEY}`);
			return res.data;
		} catch (error) {
			toast.error(error.response.data.message || error.message);
		}
	},
);
export const getForecastByLocation = createAsyncThunk(
	"weather/getForecastByLocation",
	async (coordinates) => {
        // console.log("CORDINFATEs IN APICCALL",lat,lon)
		try {
			// const res = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${location}&appid=${API_KEY}`);
			// const res = await axios.get(`api.openweathermap.org/data/2.5/forecast?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}`);
			// const res = await axios.get(`api.openweathermap.org/data/2.5/forecast?lat=44.34&lon=10.99&appid=${API_KEY}`);
			const res = await axios.get(`api.openweathermap.org/data/2.5/forecast?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}`);
			return res.data;
		} catch (error) {
			toast.error(error.response.data.message || error.message);
		}
	},
);
import { createSlice } from "@reduxjs/toolkit";
import { getForecast, getForecastByLocation } from "./forecastThunk";


const initialState = {
	loading: false,
	status: "idle",
	error: null,
	data: [],
	complete: false,
};

// A slice for CandidateCount with our 3 reducers
export const forecastSlice = createSlice({
	name: "forecast",
	initialState,
	reducers: {},
	extraReducers: {
		[getForecast.pending]: (state) => {
			state.loading = true;
		},
		[getForecast.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.status = "fulfilled";
			state.complete = true;
			state.data = payload;
		},
		[getForecast.rejected]: (state, payload) => {
			state.loading = false;
			state.error = payload;
			state.status = "rejected";
			state.complete = true;
			state.data = [];
			
		},
		[getForecastByLocation.pending]: (state) => {
			state.loading = true;
		},
		[getForecastByLocation.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.status = "fulfilled";
			state.complete = true;
			state.data = payload;
		},
		[getForecastByLocation.rejected]: (state, payload) => {
			state.loading = false;
			state.error = payload;
			state.status = "rejected";
			state.complete = true;
			state.data = [];
			
		},
	},
});

// The reducer

export const forecastReducer = forecastSlice.reducer;
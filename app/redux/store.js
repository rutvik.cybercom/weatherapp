import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { weatherReducer } from "./weatherApi/weatherSlice";
import { forecastReducer } from "./forecastApi/forecastSlice";
import { searchReducer } from "./searchApi/searchSlice";

const combinedReducer = combineReducers({
  weather: weatherReducer,
  forecast: forecastReducer,
  search: searchReducer,
});

const rootReducer = (state, action) => {
  if (action.type === "auth/logOutUser") {
    state = undefined;
  }
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
});

import { createSlice } from "@reduxjs/toolkit";
import { getSearchSuggestions } from "./searchThunk";

const initialState = {
	loading: false,
	status: "idle",
	error: null,
	data: [],
	complete: false,
};

// A slice for CandidateCount with our 3 reducers
export const searchSlice = createSlice({
	name: "search",
	initialState,
	reducers: {},
	extraReducers: {
		[getSearchSuggestions.pending]: (state) => {
			state.loading = true;
		},
		[getSearchSuggestions.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.status = "fulfilled";
			state.complete = true;
			state.data = payload;
		},
		[getSearchSuggestions.rejected]: (state, payload) => {
			state.loading = false;
			state.error = payload;
			state.status = "rejected";
			state.complete = true;
			state.data = [];
			
		},
	
	},
});

// The reducer

export const searchReducer = searchSlice.reducer;
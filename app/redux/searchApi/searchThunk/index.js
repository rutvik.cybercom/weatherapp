import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


export const getSearchSuggestions = createAsyncThunk(
	"weather/getSearchSuggestions",
	async (query) => {
		try {
			const res = await axios.get(`https://api.teleport.org/api/cities/?search=${query}&limit=5`);
			return res.data;
		} catch (error) {
			toast.error(error.response.data.message || error.message);
		}
	},
);
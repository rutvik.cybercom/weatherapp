import { createSlice } from "@reduxjs/toolkit";
import { getSearchCityWeather, getSearchCityWeatherByLocation } from "./weatherThunk";


const initialState = {
	loading: false,
	status: "idle",
	error: null,
	data: [],
	complete: false,
};

// A slice for CandidateCount with our 3 reducers
export const weatherSlice = createSlice({
	name: "weather",
	initialState,
	reducers: {},
	extraReducers: {
		[getSearchCityWeather.pending]: (state) => {
			state.loading = true;
		},
		[getSearchCityWeather.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.status = "fulfilled";
			state.complete = true;
			state.data = payload;
		},
		[getSearchCityWeather.rejected]: (state, payload) => {
			state.loading = false;
			state.error = payload;
			state.status = "rejected";
			state.complete = true;
			state.data = [];
			
		},
		[getSearchCityWeatherByLocation.pending]: (state) => {
			state.loading = true;
		},
		[getSearchCityWeatherByLocation.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.status = "fulfilled";
			state.complete = true;
			state.data = payload;
		},
		[getSearchCityWeatherByLocation.rejected]: (state, payload) => {
			state.loading = false;
			state.error = payload;
			state.status = "rejected";
			state.complete = true;
			state.data = [];
			
		},
	},
});

// The reducer

export const weatherReducer = weatherSlice.reducer;
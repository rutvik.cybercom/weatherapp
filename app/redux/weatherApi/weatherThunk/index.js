import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import API_KEY from "../../../Constants/Api";


export const getSearchCityWeather = createAsyncThunk(
	"weather/getSearchCityWeather",
	async (location) => {
		try {
			const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${API_KEY}`);
			return res.data;
		} catch (error) {
			toast.error(error.response.data.message || error.message);
		}
	},
);
export const getSearchCityWeatherByLocation = createAsyncThunk(
	"weather/getSearchCityWeatherByLocation",
	async (coordinates) => {
		try {
			const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}`);
			return res.data;
		} catch (error) {
			toast.error(error.response.data.message || error.message);
		}
	},
);

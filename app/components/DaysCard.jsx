import { Card, Typography } from '@mui/material'
import React from 'react'
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import moment from 'moment/moment';

function DaysCard({data}) {
  const { max, min,dt,dt_txt,weather}  = data
  const date = moment.unix(dt).format('DD/MM/YYYY');
  const day = moment(dt_txt).format('dddd');
  // console.log("DATA+++====>>",data)
  return (
    <Card style={{borderRadius:"35px",width:'130px',height:'160px'}} >
        <div style={{backgroundColor:'#5E82F4'}}>
            <Typography style={{textAlign:'center',color:'white'}}>{date}</Typography>
            <Typography style={{textAlign:'center',color:'white'}}>{day}</Typography>
        </div>
        <div>
            <Typography style={{textAlign:'center',color:'#5E82F4',justifyContent:'center',alignItems:'center',display:'flex',marginBottom:'10px',marginTop:'10px'}}><WbSunnyIcon style={{marginRight:'7px'}}/>{Math.ceil(max-273.15)+"° C"}</Typography>
            <Typography style={{textAlign:'center',color:'#5E82F4',justifyContent:'center',alignItems:'center',display:'flex',marginBottom:'10px'}}><WbSunnyIcon style={{marginRight:'7px'}}/>{Math.floor(min-273.15)+"° C"}</Typography>
            <Typography style={{textAlign:'center',color:'#5E82F4',marginBottom:'10px'}}>{weather[0]?.main}</Typography>
        </div>
    </Card>
  )
}

export default DaysCard
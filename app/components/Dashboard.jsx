import React, { useEffect } from "react";
import {
  Card,
  Container,
  Grid,
  IconButton,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { useState } from "react";
import LoopIcon from "@mui/icons-material/Loop";
import DaysCard from "./DaysCard";
import Carousel from "react-material-ui-carousel";
import moment from "moment";



function Dashboard({ data, handleWeatherSearch, forecastData }) {


  const forecastsByDate = {};

  forecastData?.list?.forEach((forecast) => {
    // Extract the date from the dt_txt property (assumes it's in the format "YYYY-MM-DD HH:mm:ss")
    const date = forecast.dt_txt.split(' ')[0];

    // If the date is not already in the forecastsByDate object, create an empty object for it
    if (!forecastsByDate[date]) {
      forecastsByDate[date] = { max: -Infinity, min: Infinity };
    }

    // Extract the max and min temperature from the forecast data
    const { temp_max, temp_min } = forecast.main;
    const {dt_txt,dt,weather} = forecast

    // Update max and min values if they are higher or lower than the existing values
    if (temp_max > forecastsByDate[date].max) {
      forecastsByDate[date].max = temp_max;
      forecastsByDate[date].dt_txt = dt_txt
      forecastsByDate[date].dt = dt
      forecastsByDate[date].weather = weather
      
    }
    if (temp_min < forecastsByDate[date].min) {
      forecastsByDate[date].min = temp_min;
    }
  });

  const keys = Object.keys(forecastsByDate);
  // console.log("forecastsByDate",forecastsByDate);

  return (
    <Container sx={{ mt: 4 }}>
      <Stack direction="row" spacing={5}>
        <Paper sx={{ width: "400px", borderRadius: "16px" }} elevation={12}>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: "100%",
            }}
          >
            <IconButton>
              <LoopIcon
                sx={{ color: "#5E82F4" }}
                onClick={(e) => {
                  handleWeatherSearch(e);
                }}
              />
            </IconButton>
          </div>
          <Typography
            variant="h2"
            style={{ textAlign: "center", color: "#5E82F4" }}
          >
            {data?.name || ""}
          </Typography>
          <Typography
            variant="h1"
            style={{ textAlign: "center", color: "#5E82F4" }}
          >
            {(data?.main?.temp - 273.15).toFixed(2) + "°C" || ""}
          </Typography>
          <Typography
            variant="h2"
            style={{ textAlign: "center", color: "#5E82F4" }}
          >
            {data?.weather?.[0]?.main || ""}
          </Typography>
        </Paper>
        <Paper
          sx={{ width: "400px", display: "flex", borderRadius: "16px" }}
          elevation={24}
        >
          <div style={{ width: "100%", margin: "9px" }}>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", color: "#5E82F4", margin: "16px" }}
            >
              Felt temp.
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "#5E82F4" }}
            >
              Humidity
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "#5E82F4" }}
            >
              Wind
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "#5E82F4" }}
            >
              Visibility
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "#5E82F4" }}
            >
              Max temp.
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "#5E82F4" }}
            >
              Min temp.
            </Typography>
          </div>
          <Card
            style={{
              width: "100%",
              backgroundColor: "#5E82F4",
              margin: "9px",
              borderRadius: "24px",
            }}
          >
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {(data?.main?.feels_like - 273.15).toFixed(2) + "° C" || ""}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {data?.main?.humidity + "%" || ""}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {data?.wind?.speed + " Km/h" || ""}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {(data?.visibility / 1000).toFixed(2) + " Km" || ""}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {/* {(data?.main?.temp_max - 273.15).toFixed(2) + "°C" || ""} */}
              {(forecastsByDate[keys[0]]?.max -273.15).toFixed(2) + "° C" || ""}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ textAlign: "left", margin: "16px", color: "white" }}
            >
              {/* {(data?.main?.temp_min - 273.15).toFixed(2) + "° C" || ""} */}
              {(forecastsByDate[keys[0]]?.min -273.15).toFixed(2) + "° C" || ""}
            </Typography>
          </Card>
        </Paper>

        <Paper sx={{ width: "400px", borderRadius: "16px" }} elevation={24}>
          <iframe
            src={`https://maps.google.com/maps?q=${data?.name}&t=&z=13&ie=UTF8&iwloc=&output=embed`}
            width="100%"
            height="100%"
            frameBorder="0"
            class="giphy-embed"
            allowFullScreen
            style={{ borderRadius: "10px" }}
          ></iframe>
        </Paper>
      </Stack>
      <Grid container spacing={2} style={{ marginTop: "40px" }}>
        {/* <Carousel animation="slide"> */}
{data && data?.name && 
          keys.map((item, index) => (
            <Grid item xs={2} key={index}>
              <DaysCard data={forecastsByDate[item]} />
            </Grid>
          ))}
        {/* </Carousel> */}
      </Grid>
    </Container>
  );
}

export default Dashboard;

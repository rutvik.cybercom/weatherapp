import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";
import Button from "@mui/material/Button";
import PlaceIcon from "@mui/icons-material/Place";
import { List, ListItem, ListItemText, Paper } from "@mui/material";
import { TextField } from "@mui/material";
import algoliasearch from "algoliasearch/lite";
import { useDispatch, useSelector } from "react-redux";
import { getSearchSuggestions } from "../redux/searchApi/searchThunk";
import { getSearchCityWeather } from "../redux/weatherApi/weatherThunk";
import { getForecast } from "../redux/forecastApi/forecastThunk";
import Autocomplete from "@mui/material/Autocomplete";
import CloseIcon from "@mui/icons-material/Close";
import { useState, useEffect } from "react";
import "./Header.css";
import { useFormik } from "formik";

const SearchContainer = styled("div")({
  display: "flex",
  alignItems: "stretch",
});

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    width: "auto",
  },
}));

const SearchButton = styled(Button)({
  borderTopRightRadius: "10px",
  borderBottomRightRadius: "10px",
});

const SuggestionsPaper = styled(Paper)({
  position: "absolute",
  zIndex: 1,
  marginTop: "8px",
  left: 0,
  right: 0,
});

const ClearButton = styled(Button)({
  position: "absolute",
  top: 0,
  right: 0,
  width: "36px",
  height: "100%",
  borderTopRightRadius: "10px",
  borderBottomRightRadius: "10px",
});

const ClearIcon = styled("span")({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
});

export default function Header({
  location,
  setLocation,
  handleWeatherSearch,
  handleYourLocationWeather,
}) {
  const searchClient = algoliasearch(
    "4EUZ3WRALT",
    "813597627eb785fa40167e1c50495ca3"
  );
  const [suggestions, setSuggestions] = React.useState([]);
  const searchSuggestions = useSelector((state) => state.search.data);
  const [showSuggestions, setShowSuggestions] = React.useState(false);
  const dispatch = useDispatch();
  const [focusedIndex, setFocusedIndex] = useState(-1);
  // const index = searchClient.initIndex("your_index_name");

  // const formik = useFormik({
  //   initialValues: {
  //     city: "",
  //   },
  //   onSubmit: async (values) => {
  //     e.preventDefault();
  //     handleWeatherSearch(e);
  //     // console.log("HANDLE SUBMIT CALLED");
  //   },
  // });
  // const  {handleChange, handleSubmit} = formik
  const handleSubmit = (e) => {
    e.preventDefault();
    handleWeatherSearch(e);
    // console.log("HANDLE SUBMIT CALLED");
  };
  const handleClearClick = () => {
    setLocation("");
  };

  const handleInputChange = async (e) => {
    // console.log("INPUT CHANEGE", e.target.value);
    const query = e.target.value;
    setLocation(query);
    if (query.length > 0) {
      setShowSuggestions(true);
      dispatch(getSearchSuggestions(query));
      setSuggestions(searchSuggestions?._embedded?.["city:search-results"]);
    } else {
      setSuggestions([]);
    }
  };
  const handleSuggestionClick = async (e, suggestion) => {
    // e.preventDefault()
    // console.log("handlesuggestion", suggestion);
    setShowSuggestions(false);
    await setLocation(suggestion?.matching_full_name);
    const searchWord = suggestion?.matching_full_name.split(",");
    // console.log("searchword", searchWord);
    // await handleWeatherSearch(e)
    setLocation(searchWord[0]);
    dispatch(getSearchCityWeather(searchWord[0]));
    dispatch(getForecast(searchWord[0]));
  };
  // const handleKeyDown = (e) => {
  //   console.log("KEY DOWN");
  //   if (
  //     searchSuggestions &&
  //     searchSuggestions.city &&
  //     searchSuggestions.city["search-results"]
  //   ) {
  //     const numSuggestions = searchSuggestions.city["search-results"].length;

  //     if (e.key === "ArrowDown" && focusedIndex < numSuggestions - 1) {
  //       e.preventDefault(); // Prevent the default behavior of the down arrow key
  //       setFocusedIndex((prevIndex) => prevIndex + 1);
  //     } else if (e.key === "ArrowUp" && focusedIndex > 0) {
  //       e.preventDefault(); // Prevent the default behavior of the up arrow key
  //       setFocusedIndex((prevIndex) => prevIndex - 1);
  //     }
  //   }
  // };

  // useEffect(() => {
  //   // Reset the focused suggestion when the search input changes
  //   setFocusedIndex(-1);
  // }, [location]);

  // console.log("Cities", suggestions);
  return (
    <Box sx={{ flexGrow: 1 }}>
      <form action="" onSubmit={handleSubmit}>
        <AppBar position="static" style={{ backgroundColor: "#D7DEFA" }}>
          <Toolbar style={{ marginLeft: "500px" }}>
            <SearchContainer>
              <Search>
                <TextField
                  fullWidth
                  autoComplete="off"
                  label="City"
                  name="search"
                  variant="outlined"
                  value={location}
                  size="small"
                  style={{
                    backgroundColor: "white",
                    borderTopLeftRadius: "10px",
                    borderBottomLeftRadius: "10px",
                  }}
                  onChange={(e) => {
                    handleInputChange(e);
                  }}
                  // onKeyDown={handleKeyDown}
                />
                {location.length > 0 && (
                  <ClearButton onClick={handleClearClick}>
                    <CloseIcon />
                  </ClearButton>
                )}
                {searchSuggestions?._embedded?.["city:search-results"]?.length >
                  0 &&
                  showSuggestions && (
                    <SuggestionsPaper elevation={3}>
                      <List>
                        {searchSuggestions?._embedded?.[
                          "city:search-results"
                        ]?.map((suggestion, index) => (
                          <ListItem
                            key={index}
                            button
                            onClick={(e) =>
                              handleSuggestionClick(e, suggestion)
                            }
                            className={index === focusedIndex ? "focused" : ""}
                          >
                            <ListItemText
                              primary={suggestion?.matching_full_name}
                            />
                          </ListItem>
                        ))}
                      </List>
                    </SuggestionsPaper>
                  )}
              </Search>
              <SearchButton
                variant="contained"
                type="submit"
                style={{ backgroundColor: "#5E82F4" }}
              >
                Search
              </SearchButton>
            </SearchContainer>
            {/* <Autocomplete
              fullWidth
              autoComplete="off"
              options={suggestions || []}
              getOptionLabel={(option) => option.matching_full_name || ""}
              value={location}
              size="small"
              style={{
                backgroundColor: "white",
                borderTopLeftRadius: "10px",
                borderBottomLeftRadius: "10px",
              }}
              onChange={
                handleChange
              }}
              // onKeyDown={handleKeyDown}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="City"
                  name="search"
                  variant="outlined"
                  // value={location}
                  onChange={(e) => {
                    handleInputChange(e);
                  }}
                />
              )}
            /> */}

            <Button
              variant="contained"
              size="large"
              style={{
                borderRadius: "10px",
                height: "37px",
                marginLeft: "20px",
                backgroundColor: "#5E82F4",
              }}
              onClick={handleYourLocationWeather}
            >
              <PlaceIcon /> Your Location Weather
            </Button>
          </Toolbar>
        </AppBar>
      </form>
    </Box>
  );
}
